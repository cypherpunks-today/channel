TECHNODELIC
Technodelic is an open-source typeface based on the 1981 YMO album cover art and logo designed by Yukimasa Okumura: en.wikipedia.org/wiki/Technodelic


LICENSE
This Font Software is licensed under the SIL Open Font License, Version 1.1. This license is copied above, and is also available with a FAQ at: http://scripts.sil.org/OFL


AUTHORS
Technodelic has been authored by Jonathan Maghen of Primary Foundry (primary-foundry.com) based on the original 1981 design by Yukimasa Okumura of The Studio Tokyo Japan, Inc (tstj-inc.co.jp)


DESIGN
The limited A–Z character set serves as a foundation for future iterations while also maintaining reverence for the original, which does not include lowercase, lining figures, punctuation, or symbols: primary-foundry.com/typefaces/technodelic


***
