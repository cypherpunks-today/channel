IOTA DISPLAY 
Iota Display is a modular typeface that is constructed using a series of plus and multiplication signs, which is intended to be a way of visually representing the potential for positive connections between people within a group or network. To express this idea further, the typeface is designed to expand and rotate each element with interaction and input from the viewer. Iota Display has two families, A and B, which both have 6 weights — light, book, medium, semibold, bold, and black: http://primary-foundry.com/typefaces/iota-display/


LICENSE
This Font Software is licensed under the SIL Open Font License, Version 1.1. This license is copied above, and is also available with a FAQ at: http://scripts.sil.org/OFL


AUTHORS
Iota Display has been authored by Eun Jung Bahng (jenniferbahng.com) and published by Primary Foundry (primary-foundry.com)


DESIGN
Iota Display was created using Glyphs and the Dinamo Font Gauntlet (fontgauntlet.com)


***
