#!/bin/bash

# Store the script path in a variable
DIR=$( cd -- "$( dirname "$(readlink -f "$0")" )" &> /dev/null && pwd )

pqiv --scale-mode-screen-fraction=1.0 -F ${DIR}/end-screen.mov -sci -d 22 -P 1920,0 --end-of-files-action=quit & mpv --audio-samplerate=88200 --no-video ${DIR}/end-screen.mov --volume=50
